/* eslint-disable no-unused-vars */

///////////////////////////////////////////////////////////////////
// HIGHER ORDER FUNCTIONS
//

// import './memoize';
// import './filter-factory';

///////////////////////////////////////////////////////////////////
// IMMUTABLE DATA STRUCTURES
//

// import './titlefy-nok'; // Don't try this at home.
// import './titlefy-ok';

///////////////////////////////////////////////////////////////////
// PURE FUNCTIONS
//

// import './discount1-nok';
// import './discount1-ok';

// import './discount2-nok';
// import './discount2-ok';

// KISS -> KEEP IT SIMPLE, STUPID
// kiss -> keep it stupid simple

///////////////////////////////////////////////////////////////////
// CURRYING AND PARTIAL APPLICATION
//

// import './replace1';
// import './replace2';

///////////////////////////////////////////////////////////////////
// FUNCTION COMPOSITION
//

// import './sum1';
// import './sum2';
// import './extension';
// import './prices';

// import './players';
import './mimetype';

