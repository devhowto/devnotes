#
# A graph in which the main collection is not a list, like the
# previous examples, but a dict.
#

N = {
        'a': set('bcdef'),
        'b': set('ce'),
        'c': set('d'),
        'd': set('e'),
        'e': set('f'),
        'f': set('cgh'),
        'g': set('fh'),
        'h': set('fg')
    }
